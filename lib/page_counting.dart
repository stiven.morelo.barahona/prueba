import 'package:flutter/material.dart';

class PageCounting extends StatefulWidget {
  const PageCounting({Key? key}) : super(key: key);

  @override
  State<PageCounting> createState() => _PageCountingState();
}

class _PageCountingState extends State<PageCounting> {
  int value = 0;

  void incremented() {
    setState(() {
      value++;
    });
  }

  void decremented() {
    setState(() {
      value--;
    });
  }

  void exposurezero() {
    setState(() {
      value = 0;
    });
  }

  final space = const SizedBox(
    width: 40,
    height: 20,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: Text(
            value.toString(),
            style: TextStyle(
                fontSize: 26,
                color: Colors.cyan[700],
                fontWeight: FontWeight.bold),
          ),
        ),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FloatingActionButton(
                child: const Icon(
                  Icons.add,
                  size: 32,
                ),
                onPressed: () => incremented()),
            space,
            FloatingActionButton(
                child: const Icon(
                  Icons.remove,
                  size: 32,
                ),
                onPressed: () => decremented()),
            space,
            FloatingActionButton(
                child: const Icon(
                  Icons.exposure_zero,
                  size: 32,
                ),
                onPressed: () => exposurezero()),
          ],
        ));
  }
}
